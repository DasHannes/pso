{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE FlexibleContexts #-}


module Main where

import Control.Applicative
import Data.List
import Data.List.Split
import Debug.Trace
import System.IO
import System.Random
import Control.Monad
import Control.Monad.IO.Class

import Data.IORef
import Foreign.Storable

import qualified Graphics.UI.Gtk as Gtk
import Graphics.UI.Gtk (AttrOp((:=)), on, after)
import qualified Graphics.UI.Gtk.OpenGL as GtkGL
import Graphics.Rendering.OpenGL as GL hiding (tessellate)

import Pso.GL
import Pso.GL.Types
import Pso.Math
import Pso.Math.Types
import Pso.Settings

(#>) = on
(>#) = on

defaultSwarm :: (Random k, VSpace [k] k) => ([k] -> k) -> IO (Swarm [k] k)
defaultSwarm func = randomSwarm func 500 (randList 2 {-2 comps-}) (-0.00,0.00) (-1,1)  
  

inputCell :: Gtk.WidgetClass w => String -> w -> IO Gtk.HBox
inputCell labelText widget = do
  box <- Gtk.hBoxNew True 1
  let pack w = Gtk.boxPackStart box w Gtk.PackNatural 1

  label <- Gtk.labelNew $ Just labelText
  pack label 
  pack widget
  return box


  
mkGui :: (Ord f, Random f, Enum f, Show f, Storable f, Floating f) => IORef (Settings [f] f) -> Gtk.Window -> GtkGL.GLDrawingArea -> IO ()
mkGui refSettings window canvas = do
  pane <- Gtk.hPanedNew 
  Gtk.panedPack1 pane canvas True False

  ctrlBox <- Gtk.vBoxNew False 1
  Gtk.panedPack2 pane ctrlBox  False False

  btnResetCam <- Gtk.buttonNewWithLabel "Reset Cam"
  btnResetSwarm <- Gtk.buttonNewWithLabel "Reset Swarm"
  forM_ [btnResetCam,btnResetSwarm] $ 
    \btn -> Gtk.boxPackStart ctrlBox btn Gtk.PackNatural 1

  settings <- get refSettings

  adj <- Gtk.adjustmentNew (fromIntegral $ stepCount settings) 1 50000 1 10 0
  spinSteps <- Gtk.spinButtonNew adj 1 0
  cellSteps <- inputCell "Steps: " spinSteps 

  adj <- Gtk.adjustmentNew (fromIntegral $ particleCount settings) 1 5000 1 10 0
  spinPartCount <- Gtk.spinButtonNew adj 1 0
  cellPartCount <- inputCell "Particles: " spinSteps 

  let mkCheckBox label initf transform = Gtk.toggleButtonNewWithLabel label >>= \btn -> do
      settings <- get refSettings
      Gtk.toggleButtonSetMode btn True 
      Gtk.toggleButtonSetActive btn (initf settings)
      btn #> Gtk.toggled $ do
        val <- Gtk.toggleButtonGetActive btn
        refSettings $~ transform val
        Gtk.widgetQueueDraw canvas
      return btn

  checkLocalAttractors <- mkCheckBox "Show Local Attractors" showLocalAttractors (\v s -> s { showLocalAttractors = v})
  checkShowParticles <- mkCheckBox "Show Particles" showParticles (\v s -> s {showParticles = v})
  checkNormals <- mkCheckBox "Show Normals" showNormals (\v s -> s { showNormals = v}) 

  checkShowMesh <- Gtk.toggleButtonNewWithLabel "Show Mesh"
  checkShowMesh #> Gtk.toggled $ do
    show <- Gtk.toggleButtonGetActive checkShowMesh
    s <- get refSettings
    if show then do
      putStrLn "mklines"
      mesh <- makeMeshLines (targetFunction s) (meshPrecision s) (meshDensity s)
      refSettings $= s { meshLines = mesh }
    else do
      putStrLn "rm lines"
      refSettings $= s { meshLines = []}
    Gtk.widgetQueueDraw canvas
  Gtk.toggleButtonSetActive checkShowMesh True

  comboFuncs <- Gtk.comboBoxNewText
  sequence_ $ do 
    (name,_,_) <- targetFuncs
    [Gtk.comboBoxAppendText comboFuncs name]

  comboFuncs #> Gtk.changed $ do
    i <- Gtk.comboBoxGetActive comboFuncs 
    let (_,func',scale) = targetFuncs !! i
    let func x =  (func' x) / scale
    s <- get refSettings
    mesh <- tessellate func (tesselXs s) (tesselYs s) 
    meshLines <- makeMeshLines func (meshPrecision s) (meshDensity s)
    swarm <- defaultSwarm func
    refSettings $= s { 
      mesh = Just mesh,
      meshLines = meshLines,
      targetFunction = infBounds func, 
      zScale = scale,
      swarm = Just swarm }
    Gtk.widgetQueueDraw canvas

  Gtk.set comboFuncs [ Gtk.comboBoxActive := 0]

  let add widget = Gtk.boxPackStart ctrlBox widget Gtk.PackNatural 1
  add btnResetCam
  add btnResetSwarm
  add spinPartCount
  add cellSteps
  add cellPartCount
  add checkLocalAttractors
  add checkShowMesh
  add checkNormals
  add checkShowParticles
  add comboFuncs
    
  Gtk.containerAdd window pane
  return ()
  

main :: IO ()
main = do
  let settings :: Settings [GLdouble] GLdouble = settingsDefault 
  refSettings <- newIORef settings

  Gtk.initGUI
  GtkGL.initGL

  glconfig <- GtkGL.glConfigNew [
      GtkGL.GLModeRGBA, GtkGL.GLModeDepth, GtkGL.GLModeDouble
    ]

  canvas <- GtkGL.glDrawingAreaNew glconfig

  displayFunc <- newIORef $ return ()

  displayFunc $= do
    (w,h) <- Gtk.widgetGetSize canvas
    setupView w h 
    displayFunc $= do
      (width,height) <- Gtk.widgetGetSize canvas
      display refSettings width height
    join ( GL.get displayFunc)

  canvas `on` Gtk.exposeEvent $ do
    liftIO $ GtkGL.withGLDrawingArea canvas $ \glwindow -> do 
      join (GL.get displayFunc)
      GtkGL.glDrawableSwapBuffers glwindow
    return True

  clickPos <- newIORef (0,0)
  canvas #> Gtk.buttonPressEvent $ do
    btn <- Gtk.eventButton
    pos <- Gtk.eventCoordinates
    liftIO $ do 
      case btn of
        Gtk.LeftButton -> clickPos $= pos
        _ -> return ()
      Gtk.widgetQueueDraw canvas
    return True

  Gtk.widgetAddEvents canvas [Gtk.Button1MotionMask]


  canvas #> Gtk.motionNotifyEvent $ do
    (x,y) <- Gtk.eventCoordinates
    liftIO $ do
      (rx,ry) <- get clickPos
      let (dx,dy) = (x-rx,y-ry)
      clickPos $= (x,y)
      camRotateRight (0.25 * realToFrac dx)
      camRotateUp (0.25 * realToFrac dy)
      Gtk.widgetQueueDraw canvas
    return True
    
  canvas #> Gtk.scrollEvent $ do
    dir <- Gtk.eventScrollDirection
    mods <- Gtk.eventModifier
    liftIO $ do
      case dir of
        Gtk.ScrollUp    -> camMoveForward 0.3
        Gtk.ScrollDown  -> camMoveForward (-0.3)
        Gtk.ScrollLeft  -> camRotateRight 1.0
        Gtk.ScrollRight -> camRotateRight (-1.0)
      Gtk.widgetQueueDraw canvas
    return True
 
  window <- Gtk.windowNew
  Gtk.onDestroy window Gtk.mainQuit
  -- Gtk.containerAdd window canvas

  mkGui refSettings window canvas

  window #>  Gtk.keyPressEvent $ do
    key <- Gtk.eventKeyName
    liftIO $ do
      case key of

        "Home" -> setupCam

        "k" -> do
          s <- get refSettings
          case swarm s of 
            Just curSwarm -> do
              swarm' <- stepSwarm (targetFunction s) curSwarm 
              refSettings $= s { swarm = Just swarm' }
            Nothing -> return ()

        "r" -> do
          s <- get refSettings
          newSwarm <- defaultSwarm (targetFunction s)
          refSettings $= s { swarm = Just newSwarm}

        _ -> return ()

      Gtk.widgetQueueDraw canvas
    return True


  Gtk.set window [ Gtk.containerBorderWidth := 0,
                   Gtk.windowTitle := "Gtk2Hs + OpenGL demo",
                   Gtk.windowDefaultWidth := 900,
                   Gtk.windowDefaultHeight := 900 ]

  Gtk.widgetShowAll window
  Gtk.mainGUI


