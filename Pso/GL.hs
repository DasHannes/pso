{-# LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}

module Pso.GL where

import Pso.GL.Types as T
import Pso.Math.Types as T
import Pso.Settings
import Debug.Trace
import Data.Array as A
import Data.List
import Pso.Math
import Data.List.Split
import Graphics.Rendering.OpenGL as GL
import Foreign.Marshal.Array
import Foreign.Ptr
import Foreign
import Foreign.C.Types
import Data.IORef
import Control.Applicative
import Control.Monad
import Control.Monad.Reader as R
import Data.List

third (_,_,x) = x
color3 r g b =  (Color3 r g b) :: Color3 GLdouble

vert3 :: ( Fractional f) => f -> f -> f -> Vertex3 f
vert3 x y z = Vertex3 (x) (y) (z)

class Renderable g where
  render :: g -> IO ()

instance VertexComponent f =>  Renderable ([f],f) where
  render ((x:y:_),z) = liftIO $ GL.vertex $ GL.Vertex3 x y z

class ArrayGeo a f | a -> f where
  vertsDescriptor :: a -> GL.VertexArrayDescriptor f
  elementCount    :: a -> NumArrayIndices
  primitiveMode   :: a -> GL.PrimitiveMode

class ArrayGeo a f => IndexedGeo  a f where
  idxsDescriptor :: a -> GL.VertexArrayDescriptor GLint

class IndexedGeo a f => SurfaceGeo a f where
  normalsDescriptor :: a -> GL.VertexArrayDescriptor f


instance ArrayGeo  (QuadMesh GLdouble) GLdouble where
  vertsDescriptor   qm = GL.VertexArrayDescriptor 3 GL.Double 0 (qmVertexArray qm)
  elementCount         =  qmFaceCount
  primitiveMode     qm = GL.Quads

instance ArrayGeo  (QuadMesh GLfloat) GLfloat where
  vertsDescriptor   qm = GL.VertexArrayDescriptor 3 GL.Float 0 (qmVertexArray qm)
  elementCount         = qmFaceCount
  primitiveMode     qm = GL.Quads

instance IndexedGeo (QuadMesh GLdouble) GLdouble where
  idxsDescriptor    qm = GL.VertexArrayDescriptor 1 GL.UnsignedInt 0 (qmIndexArray qm)

instance IndexedGeo (QuadMesh GLfloat) GLfloat where
  idxsDescriptor    qm = GL.VertexArrayDescriptor 1 GL.UnsignedInt 0 (qmIndexArray qm)

instance SurfaceGeo (QuadMesh GLdouble) GLdouble where
  normalsDescriptor qm = GL.VertexArrayDescriptor 3 GL.Double 0 (qmNormalArray qm)

instance SurfaceGeo (QuadMesh GLfloat) GLfloat where
  normalsDescriptor qm = GL.VertexArrayDescriptor 3 GL.Float 0 (qmNormalArray qm)

instance ArrayGeo (LineStrip GLdouble) GLdouble where
  vertsDescriptor   l  = GL.VertexArrayDescriptor 3 GL.Double 0 (lsVertexArray l)
  elementCount         = lsVertexCount 
  primitiveMode     l  = GL.LineStrip

instance ArrayGeo (LineStrip GLfloat) GLfloat where
  vertsDescriptor   l  = GL.VertexArrayDescriptor 3 GL.Double 0 (lsVertexArray l)
  elementCount         = lsVertexCount 
  primitiveMode     l  = GL.LineStrip


instance ArrayGeo (LineStrip f) f => Renderable (LineStrip f) where
  render line = do 
    GL.clientState GL.VertexArray $= Enabled
    GL.arrayPointer GL.VertexArray $= vertsDescriptor line
    GL.drawArrays (primitiveMode line) 0 (elementCount line)

instance  SurfaceGeo g f => Renderable g where
  render surf = do
    GL.clientState GL.VertexArray $= Enabled
    GL.clientState GL.NormalArray $= Enabled
    GL.arrayPointer GL.VertexArray $= vertsDescriptor surf 
    GL.arrayPointer GL.NormalArray $= normalsDescriptor surf
    let VertexArrayDescriptor comps dtype stride idxs = idxsDescriptor surf 
    GL.drawElements (primitiveMode surf) (elementCount surf) dtype idxs 
    glErrors "draw elems"

instance Renderable (Particle v k) => Renderable (Swarm v k) where
  render swarm = sequence_ $ map render (swarmParticles swarm)

instance (Fractional f, Real f, VertexComponent f) => Renderable (Vec3 f) where
  render (Vec3 x y z) = vertex $ vert3 x y z

instance (Real k, Fractional k, VertexComponent k) => Renderable (Particle [k] k) where
  render (Particle (x:y:_) z _ _ _)=  render $ Vec3 x y z

glErrors msg = do
    errs <- GL.get GL.errors
    sequence_ $ map putStrLn  ((msg ++) . show <$> errs)

tessellate :: (Floating a, Show a, Storable a, VSpace [a] a) => ([a] -> a) ->  [a] -> [a] -> IO (QuadMesh a)
tessellate f xs ys =
  let 
    w = length xs
    h = length ys
    w' :: GLint = fromIntegral w
    h' :: GLint = fromIntegral h

    vs = listArray (0,w*h-1) [ Vec3 x y (f [x,y]) | x <- xs, y <- ys]
    is = concat [ [i,i+1,i+w'+1,i+w'] | x <- [0..w'-1], y <- [0..h'-2], let i = y*w'+x]
    flat vs = ( vs >>= \(Vec3 x y z) -> [x, y, z])

    normal (p::Vec3 a) ns = 
      let 
        pairs = zip ns (drop 1 ns)
        total = foldl1 (.+.) [ cross (n1.-.p) (n2.-.p) | (n1,n2) <- pairs]
      in Pso.Math.normalize total 


    neighbors      = ((vs !) <$>)
    neighborsInner i = neighbors [ i-w , i+1  , i+w , i-1 , i-w]
    neighborsTop   i = neighbors [ i+1 , i+w , i-1 ]
    neighborsRight i = neighbors [ i+w , i-1  , i-w]
    neighborsLeft  i = neighbors [ i-w , i+1  , i+w]
    neighborsBott  i = neighbors [ i-1 , i-w , i+1 ]

    normalFor fneighbors i = normal (vs ! i) (fneighbors i)
    normalInner = normalFor neighborsInner
    normalTop   = normalFor neighborsTop
    normalRight = normalFor neighborsRight
    normalLeft  = normalFor neighborsLeft 
    normalBott  = normalFor neighborsBott 
    ns = array (0,w*h-1) $
      [ (       0, normal (vs!         0) [ vs!        1, vs!          w]),
        (   (w-1), normal (vs!         w) [ vs!    (w-1), vs!    (2*w-1)]),
        ( w*(h-1), normal (vs! (w*(h-1))) [ vs!(w*(h-2)), vs!(w*(h-1)+1)]),
        (   w*h-1, normal (vs!   (w*h-1)) [ vs!    (w-1), vs!    (2*w-1)])] ++
      [ (i,normalTop   i) | x <- [1..w-2],                let i = x] ++
      [ (i,normalLeft  i) |                y <- [1..h-2], let i = y*w] ++
      [ (i,normalRight i) |                y <- [1..h-2], let i = y*w+w-1] ++
      [ (i,normalBott  i) | x <- [1..w-2],                let i = x+(h-1)*w] ++
      [ (i,normalInner i) | x <- [1..w-2], y <- [1..h-2], let i = y*w+x]

    -- normal' i = normal (vs !! i) (neighbors i)
  in do
    let lvs = (A.elems vs)
    let lns = (A.elems ns)
    vPtr <- newArray $ flat lvs
    putStrLn $ show w ++ "," ++ show h
    nPtr <- newArray $ flat lns
    iPtr <- newArray is
    return $ QuadMesh (w'*h') vPtr nPtr lvs lns ((w'-1)*(h'-1)*4) iPtr 

-- makeMeshLines :: (Floating f, VertexComponent f) => (f -> f -> f) -> f -> f -> IO [LineStrip f]
makeMeshLines f prec dens = 
  let xs = [-1.0,-1.0+prec..1.0]
      ks = [-1.0,-1.0+dens..1.0]
      n  = fromIntegral $ length xs :: GLint
      line k = do
        a <- newArray $ xs >>= \x -> [x, k, (f [x,k])]  
        return $ T.LineStrip n a
      row k  = do
        a <- newArray $ xs >>= \x -> [k, x, (f [k,x])]
        return $ T.LineStrip n a
  in sequence $ (line <$> ks) ++ (row <$> ks)


gazeDir = gazeVec 1.0
gazeVec = camBase 2
pitchAxis :: (Floating c, Fractional c, MatrixComponent c) => IO (Vec3 c)
pitchAxis = camBase 0 1.0

camBase :: (Fractional f, Floating f, MatrixComponent f) => Int -> f -> IO (Vec3 f)
camBase n len  = do
  m :: GLmatrix f <- GL.get $ GL.matrix (Just $ GL.Modelview 0) 
  vs <- GL.getMatrixComponents GL.RowMajor m
  let rows = chunksOf 4 vs
  let [x ,y, z] = take 3 (rows !! n)
  return $  (Vec3 x y z) .* len

resizeView width height = do 

  GL.matrixMode $= GL.Projection
  GL.loadIdentity
  let ar = (fromIntegral width / fromIntegral height)
  -- GL.frustum (-ar) (ar) (-1.0) (1.0) (0.1) (100.0)
  GL.perspective 70.0 ar 0.1 100.0
  GL.matrixMode $= GL.Modelview 0

  GL.viewport $= (Position 0 0, Size (fromIntegral width) ( fromIntegral height))

setupCam = do
  GL.matrixMode $= GL.Modelview 0
  GL.loadIdentity
  GL.lookAt (Vertex3 (0.0) (2.5) (2.5)) (Vertex3 0.0 0.0 0.0) (Vector3 0.0 0.0 (1.0))

setupView w h = resizeView w h >> setupCam

camMoveForward l = do
  Vec3 x y z <- gazeVec l
  GL.translate $ Vector3 x y z

rad2deg r = r * pi / 180
camRotateUp deg = do
  Vec3 x y z <- pitchAxis
  GL.rotate (deg) $ Vector3 ( x::GLdouble) y z

camRotateRight deg = 
  GL.rotate (deg) $ Vector3 ( 0.0::GLdouble)   0.0   1.0  

-- display :: (Real f, Renderable (Swarm v f) v f, Fractional f, Renderable (Vec3 f) v f, Renderable (QuadMesh f) v f, Floating f, Integral i, VertexComponent f) 
--            => IORef (Settings v f) -> i -> i -> IO ()
display refSettings width height = do

  resizeView width height

  GL.lighting $= GL.Enabled
  GL.light (GL.Light 0) $= GL.Enabled
  GL.lightModelTwoSide $= GL.Enabled
  -- GL.diffuse (GL.Light 0) $= Color4 0.0 0.0 0.0 1.0
  GL.diffuse (GL.Light 0) $= Color4 1.0 1.0 1.0 1.0
  (Vec3 gazeX gazeY gazeZ)  <- gazeVec (1.0)
  GL.position (GL.Light 0) $= Vertex4 gazeX gazeY gazeZ 0
  GL.specular (GL.Light 0) $= Color4 1.0 1.0 1.0 1.0
  GL.materialDiffuse GL.FrontAndBack $= Color4 0.7 0.7 0.7 1.0
  GL.materialShininess GL.FrontAndBack $= 64
  GL.materialSpecular GL.FrontAndBack $= Color4 0.3 0.3 0.3 1.0

  GL.cullFace $= Nothing -- Just GL.Back
  GL.depthFunc $= Just GL.Less

  GL.clearColor $= GL.Color4 0.2 0.2 0.2 1.0

  GL.clear [GL.DepthBuffer, GL.ColorBuffer]

  GL.color $ color3 1.0 0.0 0.0
  -- Glut.renderObject Glut.Wireframe (Glut.Teapot 1.0)

  settings <- GL.get refSettings
  GL.shadeModel $= GL.Smooth
  

  case mesh settings of
    Just mesh -> do
      render mesh

      when (showNormals settings) $ do
        GL.shadeModel $= GL.Smooth
        GL.depthFunc $= Nothing
        GL.lighting $= GL.Disabled
        GL.renderPrimitive GL.Lines $ sequence_ $ do
            (v,n) <- zip (qmVertices mesh) (qmNormals mesh) 
            [
              GL.color $ color3 1 0 0,
              render v,
              GL.color $ color3 0 1 0, 
              render (v .+. (n .* 0.2))]

      GL.color $ color3 0 0 0
      GL.lighting $= GL.Disabled
      GL.depthFunc $= Nothing
      sequence_ $ meshLines settings >>= \line -> do
        [render line]
      
    Nothing -> return ()
    

  GL.shadeModel $= GL.Smooth
  GL.depthFunc $= Nothing
  GL.lighting $= GL.Disabled
  GL.pointSize $= 2.5
  case swarm settings of
    Just swarm ->  do

      -- render local optima
      when (showLocalAttractors settings) $ do
        renderPrimitive GL.Points $ do
          GL.color $ color3 0 0 1
          sequence_ $ (flip map) (swarmParticles swarm)  (\p ->
            let (x:y:_) = particleOptPos p
                z = particleOptimum p
            in vertex $ vert3 x y z)

      -- render particles
      renderPrimitive GL.Points $ do
        GL.color $ color3 1 0 0 
        render swarm

      -- render global optimum
      renderPrimitive GL.Points $ do
        GL.color $ color3 0 1 0
        let [x,y] = swarmOptPos swarm
        vertex $ vert3 x y (swarmOptimum swarm) 
    Nothing -> return()
  glErrors "display"
