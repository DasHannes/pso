{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ImpredicativeTypes #-}

module Pso.Math where

import Pso.Math.Types
import Pso.GL.Types

import Graphics.Rendering.OpenGL (GLdouble)

import System.Random
import Debug.Trace

import Control.Applicative
import Control.Monad

import Data.List
import qualified Data.Foldable as F



normalize :: (VSpace v k) => v -> v
normalize v = v ./ norm v

data Particle v k = Particle {
  particlePos :: v, particleY :: k, particleVel :: v, particleOptPos :: v, particleOptimum :: k
} deriving (Show)

data Swarm v k = Swarm {
  swarmParticles :: [Particle v k] ,
  swarmOptPos :: v,
  swarmOptimum :: k,
  swarmOldWeight :: k,
  swarmGlobalWeight :: k,
  swarmLocalWeight :: k
}


-- type FuncType = GLdouble -> GLdouble -> GLdouble
-- type  FuncType f = (Floating f) => f -> f -> f

targetFuncs :: (Enum f, Floating f, VSpace [f] f) => [(String, [f] -> f, f)]
targetFuncs = [
  ("Schwefel",schwefel',5000),
  ("Rosenbrock",rosenbrock',400000),
  ("Nice",nice',1),
  ("Rastrigin",rastrigin',100),
  ("Sphere",sphere,1),
  ("Evil",evil,10)
  ]
  
infBounds f x = if any ((> 1.0) . abs) x then 1/0 else f x

-- scaledfunc :: f -> f -> f -> (FuncType f) -> (FuncType f)
scaledfunc :: (VSpace v k) => v -> k -> (v -> k ) -> (v -> k)
scaledfunc xScale yScale f = f'
  where f' x = f ( x .*. xScale) / yScale
  
type FuncType = (Enum f, Floating f, VSpace [f] f) => [f] -> f

schwefel' :: FuncType
schwefel' = scaledfunc [500.0,500.0..] 1 schwefel
schwefel x = sum $ map f x 
  where f a = (-a) * sin (sqrt (abs a))

rastrigin' :: FuncType
rastrigin' = scaledfunc [5.12,5.12..] 1 rastrigin
rastrigin :: FuncType
rastrigin = sum . map f
  where f a = 10 + a^2 - 10 * cos (2 * pi * a)

rosenbrock' :: FuncType
rosenbrock' = scaledfunc (repeat 30) (1) rosenbrock
-- rosenbrock :: v -> k
rosenbrock :: VSpace [k] k => [k] -> k
rosenbrock x = sum $ f <$> x <*> (drop 1 x)
  where f x1 x0  = 100 * (x1-x0^2)^2 + (1-x0)^2

--- sphere :: FuncType f
sphere :: VSpace v k => v -> k
sphere x = x *.* x

--- evil :: FuncType f
-- evil' = scaledfunc 1 1 1 evil
evil x = sum $ (liftM2 f) x (drop 1 x)
  where f x' y' = 
          let x = (x'-1)
              y = (y'-1)
          in 2.1 * x^3 * y^2 - 1.5 * y^2 * x  * sin (x*y*50)

-- nice' :: Floating f => f -> f -> f
nice' :: FuncType
nice' = scaledfunc (repeat 5) 1 nice
nice :: FuncType
nice [x,y] = (-1) * (x^3 - 2.5 * y^2 * x + (x*x + y * y)) / 25 / (1.2**( x^2 + y^2 ))

--- randomParticle :: IO a -> IO a -> IO (Particle a)
randomParticle rV rP = do
  v <- rV
  x <- rP
  return (Particle x v x 0) -- FIXME: initilize optimum


-- instance (Floating r) => VSpace (Vec3 r) r where
--   (Vec3 x1 y1 z1) .+. (Vec3 x2 y2 z2) = Vec3 (x1+x2) (y1+y2) (z1+z2)
--   v *. (Vec3 x y z) = Vec3 (v*x) (v*y) (v*z)
--   (Vec3 ax ay az) *.* (Vec3 bx by bz) =  ax*bx + ay*by + az*bz
--   dimension _ = 3

instance (Floating r) => VSpace [r] r where
  (*.*) = (F.sum .) . (zipWith (*)) 
  (.+.) = zipWith (+) 
  (*.)  = liftM . (*)
  (.*.) = zipWith (*)
  elems = id


randList n range = sequence $ replicate n (randomRIO range)

-- randomSwarm :: (FromRandom (Vec3 r) r, Fractional r, Random r) => Int -> (r,r) -> (r,r) -> IO (Swarm (Vec3 r) r)
-- randomSwarm :: Int -> (GLdouble, GLdouble) -> (GLdouble, GLdouble) ->  IO ( Swarm (Vec3 GLdouble) GLdouble)
randomSwarm :: (VSpace v k) => (v -> k) -> Int -> ( (k,k) -> IO v ) -> (k,k) -> (k,k) -> IO (Swarm v k)
randomSwarm func n mkV vRange pRange = do
  ps <- particles 
  (xo,opt) <- optimum
  return $ Swarm ps xo opt 0.7 1.4 1.4
  where 
    optimum = do
      x <- mkV pRange
      return (x,func x)
    particles = replicateM n $ do
      v <- mkV vRange
      x <- mkV pRange
      let y = func x
      return $ Particle x y v x y 


type ParamSurf2 a = a -> a -> Vec3 a

cross (Vec3 x1 y1 z1) (Vec3 x2 y2 z2) = Vec3 (y1*z2 - z1*y2) (z1*x2 - x1*z2) (x1*y2 - y1*x2)

handleBounds = id

stepParticle :: (FromRandom a b, Random b, Ord b, VSpace a b) => (a -> b) -> Swarm a b -> Particle a b -> IO (Particle a b)
stepParticle func (Swarm _ global _ wOld wGlobal wLocal) (Particle x y v ox oy) = do
  rGlobal :: a <- fromRandom $ randomRIO (0.0,1.0)
  rLocal :: a <- fromRandom $ randomRIO (0.0,1.0)
  let
    v' =  v .* wOld .+. (global .-. x) .* wGlobal .*. rGlobal .+. (ox .-. x) .* wLocal .*. rLocal
    x' =  x .+. v'
    y' =  func x'
  return $ handleBounds $ 
    if y' < oy then
      Particle x' y' v' x' y' 
    else
      Particle x' y' v' ox  oy
  
compareOptima :: (Ord k) => Particle v k -> Particle v k -> Ordering
compareOptima p1 p2 = compare (particleOptimum p1) (particleOptimum p2)

stepSwarm :: (FromRandom a b, Random b, VSpace a b , Ord b ) => (a -> b) -> Swarm a b -> IO( Swarm a b)
stepSwarm f s@(Swarm ps o y wo wl wg) = 
  let 
  in do 
    ps' <- sequence $  stepParticle f s <$> ps
    let Particle _ _ _ o y =  minimumBy compareOptima ps'
    return $ s { swarmParticles = ps', swarmOptimum = y, swarmOptPos = o}

