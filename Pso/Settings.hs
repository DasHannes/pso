{-# LANGUAGE Rank2Types #-}

module Pso.Settings where

import Pso.Math
import Pso.GL.Types as T
import Graphics.Rendering.OpenGL
import Foreign.C.Types

data Settings v f = Settings {
  particleCount :: Int,
  stepCount :: Int,
  showNormals :: Bool,
  showLocalAttractors :: Bool,
  showParticles :: Bool,
  zScale :: f,
  tesselXs :: [f],
  tesselYs :: [f],
  targetFunction :: v -> f,
  mesh :: Maybe (QuadMesh f),
  swarm :: Maybe (Swarm v f),
  meshDensity :: f,
  meshPrecision :: f,
  meshLines :: [T.LineStrip f]
}

settingsDefault :: (Enum f, Floating f) => Settings [f] f
settingsDefault = Settings {
  particleCount = 100,
  stepCount = 1,
  showNormals = False,
  showLocalAttractors = False,
  showParticles = True,
  targetFunction = (\(_,f,_) -> f) $ head targetFuncs,
  zScale = (\(_,_,scale) -> scale) $ head targetFuncs,
  tesselXs = [-1.0,-0.99..1.0],
  tesselYs = [-1.0,-0.99..1.0],
  mesh = Nothing,
  swarm = Nothing,
  meshDensity = 0.2,
  meshPrecision = 0.01,
  meshLines = []
}

