{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

-- import Prelude hiding (replicate,drop)
import Control.Applicative
-- import Data.Sequence
import Data.List
import Debug.Trace
import Pso.Math
import Pso.Math.Types
import qualified Data.Vector as V 
import System.Random

nextElement = (>>= return . (+ 1)) :: IO Int -> IO Int

elements i = iterate nextElement (return i)

reportStep func (swarm,gen) = do
  swarm' <- stepSwarm func swarm
  let x' = swarmOptPos swarm'
      y' = swarmOptimum swarm' 
  putStrLn ( "gen " ++ show gen ++ " optimum: " ++ show y' ++ "\n" ++ unlines (("  " ++ ) . show <$> x')) 
  -- getLine
  return (swarm',gen+1)
  where 

-- func :: (Enum k, Floating k, VSpace [k] k) => [k] -> k
func = infBounds rastrigin'

-- nextStep :: (Floating k, Ord k, Show k) => IO (Swarm [k] k, Int) -> IO (Swarm [k] k, Int)
nextStep =  (>>= reportStep func)
steps first = iterate nextStep (return first)

main :: IO ()
main = do
  let n = 1000
  let d = 200
  swarm :: Swarm [Double] Double <- randomSwarm func 500 (randList d) (0.0,0.0) (-1,1) 
  (s,i) <- head . drop n . steps $ (swarm,0)
  
  -- putStrLn ( unlines $ (map show . (sort . fmap particleOptimum) $ swarmParticles s))
  return ()

