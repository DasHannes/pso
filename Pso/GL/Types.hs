{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

module Pso.GL.Types where

import Pso.Math.Types
import Graphics.Rendering.OpenGL
import Foreign.Ptr
import System.Random
import System.IO.Unsafe

data LineStrip f = LineStrip {
  lsVertexCount :: GLint,
  lsVertexArray :: (Ptr f)
}

data QuadMesh n = QuadMesh {
  qmVertexCount :: GLint,
  qmVertexArray :: (Ptr n),
  qmNormalArray :: (Ptr n),
  qmVertices :: [Vec3 n],
  qmNormals :: [Vec3 n],
  qmFaceCount :: GLint,
  qmIndexArray :: (Ptr GLint)
} deriving (Show)

class FromRandom a r | a -> r where
  fromRandom :: Random r => IO r -> IO a

instance FromRandom (Vec2 r) r  where
  fromRandom rnd = do
    x <- rnd; y <- rnd;
    return $ Vec2 x y

instance FromRandom (Vec3 r) r  where
  fromRandom rnd = do
    x <- rnd; y <- rnd; z <- rnd;
    return $ Vec3 x y z
instance FromRandom [k] k where
  fromRandom rnd = do
    fst <- rnd
    rest <- unsafeInterleaveIO $ fromRandom rnd
    return (fst:rest)

