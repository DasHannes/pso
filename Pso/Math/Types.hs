{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

module Pso.Math.Types where

infixl 6 .+.
infixl 6 .-.
infixl 7 .*.
infixl 7 .*
infixl 7 *.
infixl 7 ./
infixl 8 *.*

class (Floating k) => VSpace v k | v -> k where
  (.+.) :: v -> v -> v 
  (.-.) :: v -> v -> v
  l .-. r = l .+. ( r .* realToFrac (-1.0) ) 
  (*.) :: k -> v -> v
  (.*) :: v -> k -> v
  (.*) = flip (*.)
  (./) :: v -> k -> v
  v ./ s = v .* (1.0 / s)
  (*.*) :: v -> v -> k
  -- | component wise multiplication
  (.*.) :: v -> v -> v
  norm :: v -> k
  norm v = sqrt ( v *.* v)
  elems :: v -> [k]

data Vec2 n = Vec2 n n deriving (Show,Eq)
data Vec3 n = Vec3 { getX :: n, getY :: n, getZ :: n} deriving (Show,Eq)

instance (Floating f) => VSpace (Vec3 f) f where
  (Vec3 a0 a1 a2) .+. (Vec3 b0 b1 b2) = Vec3 (a0+b0) (a1+b1) (a2+b2)
  (Vec3 a0 a1 a2) .-. (Vec3 b0 b1 b2) = Vec3 (a0-b0) (a1-b1) (a2-b2)
  (Vec3 a0 a1 a2) .*. (Vec3 b0 b1 b2) = Vec3 (a0+b0) (a1+b1) (a2+b2)
  (Vec3 a0 a1 a2) *.* (Vec3 b0 b1 b2) = (a0*b0)+(a1*b1)+(a2*b2)
  k *. (Vec3 x0 x1 x2) = Vec3 (x0*k) (x1*k) (x2*k)
  elems (Vec3 x y z) = [x,y,z]

