SRCS := */*.hs
OBJS := $(SRCS:.hs=.o)
INTS := $(SRCS:.hs=.hi)
DFLAGS := -prof 
GHCFLAGS := -O3 -rtsopts # $(DFLAGS)

top : all

all : pso pso_gui

pso: $(SRCS)
	ghc --make Pso/Main.hs -o pso $(GHCFLAGS)

pso_gui : $(SRCS)
	ghc --make Pso/Gui.hs -o pso_gui $(GHCFLAGS)

clean : 
	rm -f $(OBJS) $(INTS) pso pso_gui

run : pso
	./pso
